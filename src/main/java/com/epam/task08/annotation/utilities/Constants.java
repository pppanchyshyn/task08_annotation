package com.epam.task08.annotation.utilities;

public class Constants {

  public final static String CAR_DEFAULT_BRAND = "BMW";
  public final static String CAR_NEW_BRAND = "Ford";
  public final static int CAR_DOORS_NUMBER = 4;
  public final static String[] MY_METHOD_TESTING_STRING_ARGS = {"one", "two", "three"};
  public final static int[] MY_METHOD_TESTING_INT_ARGS = {1, 2, 3};
  public final static int EXIT_MENU_ITEM = 6;

  private Constants() {
  }
}
