package com.epam.task08.annotation.controller;

import com.epam.task08.annotation.utilities.CustomAnnotation;
import java.lang.reflect.Field;
import java.util.Collection;

public class CarAnnotatedFieldsGetter extends AbstractController {

  public CarAnnotatedFieldsGetter() {
  }

  public Collection<String> execute() {
    Field[] fields = cls.getDeclaredFields();
    logger.info("Fields of the Car class, annotated with CustomAnnotation: ");
    for (Field field : fields
        ) {
      if (field.isAnnotationPresent(CustomAnnotation.class)) {
        field.setAccessible(true);
        try {
          resultsList.add(field.getName() + " - " + field.get(car));
        } catch (IllegalAccessException e) {
          e.printStackTrace();
        }
      }
    }
    return resultsList;
  }
}
