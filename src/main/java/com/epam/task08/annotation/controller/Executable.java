package com.epam.task08.annotation.controller;

import java.util.Collection;

public interface Executable {
 Collection<String> execute();
}
