package com.epam.task08.annotation.controller;

import com.epam.task08.annotation.model.Car;
import com.epam.task08.annotation.utilities.Constants;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

abstract class AbstractController implements Executable {

  Car car;
  Class<? extends Car> cls;
  Logger logger = LogManager.getLogger(AbstractController.class);
  Collection<String> resultsList;

  AbstractController() {
    car = new Car(Constants.CAR_DEFAULT_BRAND, Constants.CAR_DOORS_NUMBER, false);
    cls = car.getClass();
    resultsList = new ArrayList<>();
  }
}
