package com.epam.task08.annotation.controller;

import com.epam.task08.annotation.utilities.Constants;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;

public class CarMethodsInvoker extends AbstractController {

  public CarMethodsInvoker() {
  }

  private Object getBrand() {
    Object value = null;
    try {
      Method getBrand = cls.getMethod("getBrand");
      value = getBrand.invoke(car);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      e.printStackTrace();
    }
    return value;
  }

  private Object isBudgetary() {
    Object value = null;
    try {
      Method isBudgetary = cls.getDeclaredMethod("isBudgetary");
      isBudgetary.setAccessible(true);
      value = isBudgetary.invoke(car);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      e.printStackTrace();
    }
    return value;
  }

  private void setBrand() {
    try {
      Method setBrand = cls.getDeclaredMethod("setBrand", String.class);
      setBrand.setAccessible(true);
      setBrand.invoke(car, Constants.CAR_NEW_BRAND);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      e.printStackTrace();
    }
  }

  @Override
  public Collection<String> execute() {
    logger.info("Invoking getBrand method");
    resultsList.add(String.format("Brand - %s", getBrand()));
    logger.info("Invoking isBudgetary method");
    resultsList.add(String.format("Is it budgetary? - %s", isBudgetary()));
    logger.info("Invoking setBrand method. Set value \"Ford\"");
    setBrand();
    resultsList.add(String.format("Brand after updating - %s", getBrand()));
    return resultsList;
  }
}
