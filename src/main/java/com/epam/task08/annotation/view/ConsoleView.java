package com.epam.task08.annotation.view;

import com.epam.task08.annotation.controller.CarAnnotatedFieldsGetter;
import com.epam.task08.annotation.controller.CarAnnotationValueGetter;
import com.epam.task08.annotation.controller.CarFieldsValuesSetter;
import com.epam.task08.annotation.controller.CarMethodsInvoker;
import com.epam.task08.annotation.controller.Executable;
import com.epam.task08.annotation.controller.MyMethodInvoker;
import com.epam.task08.annotation.controller.UnknownObjectBreaker;
import com.epam.task08.annotation.utilities.Constants;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView implements Runnable {

  private Scanner input = new Scanner(System.in);
  private Logger logger = LogManager.getLogger(Application.class);
  private Map<Integer, Executable> map;

  ConsoleView() {
    map = new HashMap<>();
    map.put(0, new CarAnnotatedFieldsGetter());
    map.put(1, new CarAnnotationValueGetter());
    map.put(2, new CarMethodsInvoker());
    map.put(3, new CarFieldsValuesSetter());
    map.put(4, new MyMethodInvoker());
    map.put(5, new UnknownObjectBreaker());
  }

  private int enterInteger() {
    int integer;
    while (true) {
      if (input.hasNextInt()) {
        integer = input.nextInt();
        break;
      } else {
        System.out.print("Not integer! Try again: ");
        input.nextLine();
      }
    }
    return integer;
  }

  private int enterMenuItem() {
    int menuItem;
    while (true) {
      menuItem = enterInteger();
      if ((menuItem >= 0) && (menuItem <= Constants.EXIT_MENU_ITEM)) {
        break;
      } else {
        logger.info("Non-existent menu item. Try again: ");
        input.nextLine();
      }
    }
    return menuItem;
  }

  private void print(Collection<String> collection) {
    for (String message : collection
        ) {
      logger.info(message);
    }
  }

  private void printMenu() {
    logger.info("To print through reflection those "
        + "fields in the class that were annotate by CustomAnnotation press 0.\n"
        + "To print annotation value into console press 1\n"
        + "To print result of invoking through reflection three methods with different"
        + "parameters and return types press 2\n"
        + "To print result of setting value into fields not knowing its type press 3\n"
        + "To print result of invoking myMethod(String a, int ... args) and "
        + "myMethod(String … args) press 4\n"
        + "To print information about class of unknown object press 5\n"
        + "To exit program press 6\n"
        + "\nMake your choice: ");
  }

  @Override
  public void run() {
    int menuItem;
    while (true) {
      printMenu();
      menuItem = enterMenuItem();
      if (menuItem == Constants.EXIT_MENU_ITEM) {
        logger.info("Good luck");
        break;
      }
      logger.info("-------------------------------------------------------------------------");
      print(map.get(menuItem).execute());
      logger.info("-------------------------------------------------------------------------");
    }
  }
}
