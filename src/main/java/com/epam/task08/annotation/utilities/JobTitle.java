package com.epam.task08.annotation.utilities;

public enum JobTitle {
  STUDENT, TEACHER, UNKNOWN
}
