package com.epam.task08.annotation.controller;

import com.epam.task08.annotation.utilities.Constants;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;

public class MyMethodInvoker extends AbstractController {

  public MyMethodInvoker() {
  }

  private Object invokeMyMethod1() {
    Object value = null;
    String[] strings = Constants.MY_METHOD_TESTING_STRING_ARGS;
    try {
      Method myMethod1 = cls.getDeclaredMethod("myMethod1", String[].class);
      myMethod1.setAccessible(true);
      value = myMethod1.invoke(car, (Object) strings);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      e.printStackTrace();
    }
    return value;
  }

  private Object invokeMyMethod2() {
    Object value = null;
    int[] ints = Constants.MY_METHOD_TESTING_INT_ARGS;
    String arg = Constants.CAR_DEFAULT_BRAND;
    try {
      Method myMethod2 = cls.getDeclaredMethod("myMethod2", String.class, int[].class);
      myMethod2.setAccessible(true);
      value = myMethod2.invoke(car, arg, ints);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      e.printStackTrace();
    }
    return value;
  }

  @Override
  public Collection<String> execute() {
    logger.info("Invoking myMethod1 method");
    resultsList.add(String.format("Result of myMethod1 invoking - %s", invokeMyMethod1()));
    logger.info("Invoking myMethod2 method");
    resultsList.add(String.format("Result of  myMethod2 invoking - %s", invokeMyMethod2()));
    return resultsList;
  }
}
