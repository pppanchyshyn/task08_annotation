package com.epam.task08.annotation.controller;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;

public class UnknownObjectBreaker extends AbstractController {

  public UnknownObjectBreaker() {
  }

  private void getObjectFields() {
    Field[] fields = cls.getDeclaredFields();
    resultsList.add("All class fields:");
    for (Field field : fields
        ) {
      resultsList
          .add(Modifier.toString(field.getModifiers()) + " "
              + field.getType().getSimpleName() + " "
              + field.getName());
    }
    resultsList.add("--------------------------");
  }

  private void getObjectMethods() {
    Method[] methods = cls.getDeclaredMethods();
    resultsList.add("All class declared methods:");
    for (Method method : methods
        ) {
      resultsList
          .add(Modifier.toString(method.getModifiers()) + " "
              + method.getReturnType() + " "
              + method.getName() + " "
              + Arrays.toString(method.getParameterTypes()));
    }
    resultsList.add("--------------------------");
  }

  private void getObjectConstructors() {
    Constructor[] constructors = cls.getConstructors();
    resultsList.add("All class constructors:");
    for (Constructor constructor : constructors
        ) {
      resultsList
          .add(Modifier.toString(constructor.getModifiers()) + " "
              + constructor.getName() + " "
              + Arrays.toString(constructor.getParameterTypes()));
    }
  }

  @Override
  public Collection<String> execute() {
    getObjectFields();
    getObjectMethods();
    getObjectConstructors();
    return resultsList;
  }
}
