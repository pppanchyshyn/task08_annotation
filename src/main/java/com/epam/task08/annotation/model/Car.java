package com.epam.task08.annotation.model;

import com.epam.task08.annotation.utilities.CustomAnnotation;
import com.epam.task08.annotation.utilities.JobTitle;

public class Car {

  @CustomAnnotation()
  private String brand;
  private String color;
  @CustomAnnotation(author = "Panchyshyn", jobTitle = JobTitle.STUDENT)
  private int doorsNumber;
  @CustomAnnotation(author = "Pavelchak", jobTitle = JobTitle.TEACHER)
  private Boolean isBudgetary;

  public Car(String brand, int doorsNumber, boolean isBudgetary) {
    this.brand = brand;
    this.doorsNumber = doorsNumber;
    this.isBudgetary = isBudgetary;
    color = "black";
  }

  public String getBrand() {
    return brand;
  }

  private void setBrand(String brand) {
    this.brand = brand;
  }

  public boolean isBudgetary() {
    return isBudgetary;
  }

  private String myMethod1(String... args) {
    StringBuilder builder = new StringBuilder();
    for (String arg : args
        ) {
      builder.append(String.format("%s ", arg));
    }
    return builder.toString();
  }

  private String myMethod2(String a, int... args) {
    StringBuilder builder = new StringBuilder();
    builder.append(String.format("%s ", a));
    for (int arg : args
        ) {
      builder.append(String.format("%d ", arg));
    }
    return builder.toString();
  }
}

