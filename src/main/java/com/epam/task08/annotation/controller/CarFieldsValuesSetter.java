package com.epam.task08.annotation.controller;

import com.epam.task08.annotation.utilities.Constants;
import java.lang.reflect.Field;
import java.util.Collection;

public class CarFieldsValuesSetter extends AbstractController {

  public CarFieldsValuesSetter() {
  }

  private void setValue(String fieldName, Object value) {
    try {
      Field field = cls.getDeclaredField(fieldName);
      field.setAccessible(true);
      if (field.getType().isInstance(value)) {
        field.set(car, value);
        logger.info("Excellent value has been changed");
      } else {
        logger.warn("Incorrect type");
      }
    } catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException e) {
      e.printStackTrace();
    }
  }

  @Override
  public Collection<String> execute() {
    resultsList.add("Before setting: ");
    resultsList
        .add(String.format("Brand - %s\nIs it budgetary? - %b", car.getBrand(), car.isBudgetary()));
    logger.info("Try to change value of brand to 4");
    setValue("brand", Constants.CAR_DOORS_NUMBER);
    logger.info("Try to change value of brand to \"Ford\"");
    setValue("brand", "Ford");
    logger.info("Try to change value of isBudgetary to \"yes\"");
    setValue("isBudgetary", "yes");
    logger.info("Try to change value of isBudgetary to \"true\"");
    setValue("isBudgetary", true);
    resultsList.add("After setting: ");
    resultsList
        .add(String.format("Brand - %s\nIs it budgetary? - %b", car.getBrand(), car.isBudgetary()));
    return resultsList;
  }
}