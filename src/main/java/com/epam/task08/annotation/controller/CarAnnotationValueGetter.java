package com.epam.task08.annotation.controller;


import com.epam.task08.annotation.utilities.CustomAnnotation;
import java.lang.reflect.Field;
import java.util.Collection;

public class CarAnnotationValueGetter extends AbstractController {

  public CarAnnotationValueGetter() {
  }

  @Override
  public Collection<String> execute() {
    logger.info("CustomAnnotation values of the Car class fields: ");
    Field[] fields = cls.getDeclaredFields();
    for (Field field : fields
        ) {
      if(field.isAnnotationPresent(CustomAnnotation.class)) {
        CustomAnnotation customAnnotation = field.getDeclaredAnnotation(CustomAnnotation.class);
        resultsList.add("Field: " + field.getName() +
            " Author: " + customAnnotation.author() +
            " Job tittle: " + customAnnotation.jobTitle());
      }
    }
    return resultsList;
  }
}
